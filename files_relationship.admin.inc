<?php

function files_relationship_admin() {
  $form = array();
  $form['batch_update'] = array(
    '#type' => 'submit',
    '#value' => t('Submit batch update'),
    '#submit' => array('files_relationship_batch_update_submit'),
  );

  return $form;
}

function files_relationship_batch_update_submit($form, &$form_state) {
  // Remove all items from node/filefield.
  db_query("DELETE FROM {files_relationship} WHERE storage_info = ('%s') OR storage_info LIKE ('%s%%')", array('upload', 'cck_'));
  $batch = array(
    'title' => t('Updating files relationships'),
    'finished' => 'files_relationship_batch_update_finished',
    'file' => drupal_get_path('module', 'files_relationship') .'/files_relationship.admin.inc',
  );
  $max = db_result(db_query_range('SELECT nid FROM {node} ORDER BY nid DESC', 0, 1));
  $limit = 10;
  for ($i = 0; $i <= ceil($max/$limit); $i++) {
    $batch['operations'][] = array('files_relationship_batch_process', array($i * $limit, ($i + 1) * $limit - 1));
  }
  batch_set($batch);
}

/**
 * Process 10 nodes at the same time.
 */
function files_relationship_batch_process($start, $end, &$context) {
  // Process for upload/filefield for 10 elements.
  if (module_exists('filefield')) {
    files_relationship_batch_process_filefield($start, $end);
  }
  if (module_exists('upload')) {
    files_relationship_batch_process_upload($start, $end);
  }
}

function files_relationship_batch_process_upload($start, $end) {
  $result = db_query("SELECT nid, fid FROM {upload} WHERE nid > %d AND nid <= %d ORDER BY nid ASC", $start, $end);
  while ($upload = db_fetch_object($result)) {
    files_relationship_create($upload->nid, 'node', $upload->fid, 'upload');
  }
}

function files_relationship_batch_process_filefield($start, $end) {
  $file_fields = files_relationship_content_filefields('full');
  $result = db_query("SELECT nid, vid, type FROM {node} WHERE nid > %d AND nid <= %d ORDER BY nid ASC", $start, $end);
  while ($node_record = db_fetch_object($result)) {
    // Don't node_load twice, so the variables aren't filled up.
    $node = node_load($node_record->nid, NULL, TRUE);
    foreach ($file_fields as $name => $field) {
      if (isset($node->{$name})) {
        $items = $node->{$name};
        foreach ($items as $item) {
          files_relationship_create($node->nid, 'node', $item['fid'], 'cck_' . $name);
        }
      }
    }
  }
}

function files_relationship_batch_update_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One files relationship processed.', '@count files relationship processed.');
    drupal_set_message($message);
  }
}