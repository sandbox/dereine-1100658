<?php

/**
 * Implements hook_views_data().
 */
function files_relationship_views_data() {
  $data = array();
  // @todo: Create some hooks to register availible entity_types.
  $data['files_relationship']['table']['group'] = t('File relationships');
  $data['files_relationship']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'entity_id',
    'extra' => array(
      array('field' => 'entity_type', 'value' => 'node'),
    ),
  );
  $data['files_relationship']['table']['join']['files'] = array(
    'left_field' => 'fid',
    'field' => 'fid',
  );
  $data['files_relationship']['fid'] = array(
    'title' => t('File'),
    'help' => t('Show a single file of a file relationship'),
    'field' => array(
      'handler' => 'files_relationship_handler_field_fid',
    ),
    'relationship' => array(
      'label' => t('File'),
      'base' => 'files',
      'base field' => 'fid',
    ),
  );
  $data['files_relationship']['node'] = array(
    'title' => t('node'),
    'help' => t(''),
    'relationship' => array(
      'label' => t('Node'),
      'base' => 'node',
      'base field' => 'nid',
      'relationship field' => 'entity_id',
      'extra' => array(
        array('table' => 'files_relationship', 'field' => 'entity_type', 'value' => 'node'),
      ),
    ),
  );
  $data['files_relationship']['storage_info'] = array(
    'title' => t('Storage Info'),
    'help' => t('What for a kind of storage is used.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function files_relationship_views_data_alter(&$data) {
  $data['files']['filemime']['filter']['handler'] = 'files_relationship_handler_filter_filemime';
  $data['files']['filemime']['field']['handler'] = 'files_relationship_handler_field_filemime';
}

/**
 * Implements hook_views_handlers().
 */
function files_relationship_views_handlers() {
  return array(
    'handlers' => array(
      // Field handlers
      'files_relationship_handler_field_fid' => array(
        'parent' => 'views_handler_field',
      ),
      'files_relationship_handler_field_filemime' => array(
        'parent' => 'views_handler_field_file',
      ),
      // Filter handlers
      'files_relationship_handler_filter_filemime' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}
