<?php

/**
 * @todo
 *   create a join to files to use additional_fields.
 */
class files_relationship_handler_field_fid extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_file'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_file'] = array(
      '#title' => t('Link this field to download the file'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_file']),
    );
  }

  function query($group_by = FALSE) {
    if (!empty($this->options['link_to_file'])) {
      $join = new views_join();
      $join->construct('files', 'files_relationship', 'fid', 'fid', array(), 'INNER');
      $this->query->ensure_table('files', NULL, $join);

      $this->aliases['filename'] = $this->query->add_field('files', 'filename');
      $this->aliases['filepath'] = $this->query->add_field('files', 'filepath');
    }
    
    return parent::query($group_by);
  }

  function render(&$values) {
    return $this->render_link($values->{$this->field_alias}, $values);
  }

  function render_link($data, $values) {
    if (!empty($this->options['link_to_file'])) {
      $this->options['make_link'] = TRUE;
      $this->options['path'] = file_create_url($values->{$this->aliases['filepath']});
    }
    return $values->{$this->aliases['filename']};
  }
}