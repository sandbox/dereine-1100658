<?php
// $Id$

class files_relationship_handler_field_filemime extends views_handler_field_file {
  function render($values) {
    return t($this->render_value($values));
  }

  function render_value(&$values) {
    $value = parent::render($values);

    $value = str_replace(array('/', '.'), array('_', '-'), $value);
    $value = str_replace(array_keys($this->mimetype_aliases()), array_values($this->mimetype_aliases()), $value);
    return $value;
  }
  /**
   * Add css-usuable-tokens.
   */
  function document_self_tokens(&$tokens) {
    $tokens['[' . $this->options['id'] . '-css' . ']'] = t('Provide css usuable version of the filemime.');
  }

  function get_render_tokens($item) {
    $tokens = parent::get_render_tokens($item);
    $values =  $this->view->result[$this->view->row_index];
    $tokens["[" . $this->options['id'] . "-css]"] = strtolower(str_replace(' ', '_', $this->render_value($values)));
    return $tokens;
  }

  /**
   * @todo
   *
   * Add filetype as additional_field and load the information based on this.
   */
  function mimetype_aliases() {
    static $types = NULL;
    if (!isset($types)) {
      $types = variable_get('files_relationship_filemime_aliases', array(
        'image_jpeg' => 'Picture',
        'image_jpeg' => 'Picture',
        'image_gif' => 'Picture',
        'image_png' => 'Picture',
        'text_plain' => 'Text',
        'text_html' => 'HTML',
        'application_msword' => 'Wordfile',
        'application_vnd-ms-excel' => 'Excelfile',
        'application_vnd-ms-powerpoint' => 'Powerpointfile',
        'application_vnd-openxmlformats-officedocument-wordprocessingml-document' => 'Wordfile',
        'application_vnd-openxmlformats-officedocument-presentationml-presentation' => 'Powerpointfile',
        'application_vnd-openxmlformats-officedocument-spreadsheetml-sheet' => 'Excelfile',
        'application_pdf' => 'PDF',
        'application_vnd-oasis-opendocument-text' => 'OpenOffice Text',
        'application_vnd-oasis-opendocument-spreadsheet' => 'OpenOffice Spreadsheet',
        'application_vnd-oasis-opendocument-presentation' => 'OpenOffice Presentation',
        'audio_mpeg' => 'Audio',
        'video_mp4' => 'Video',
        'video_mpeg' => 'Video',
        'video_x-msvideo' => 'Video',
        'video_mpeg' => 'Video',
        'video_x-ms-wmv' => 'Video',
        'video_x-flv' => 'Video',
        'video_quicktime' => 'Video',
      ));
    }
    return $types;
  }
}
