<?php
// $Id$

class files_relationship_handler_filter_filemime extends views_handler_filter_in_operator {
  function get_value_options() {
    $list = array(
      'image' => t('Images'),
      'video' => t('Videos'),
      'audio' => t('Audios'),
      'documents' => t('Documents'),
    );
    $this->value_options = $list;
  }

  function operators() {
    $operators = parent::operators();
    $operators['by_type'] = array(
      'title' => t('Is one of type'),
      'short' => t('by_type'),
      'short_single' => t('='),
      'method' => 'op_by_type',
      'values' => 1,
    );

    return $operators;
  }

  /**
   * Extend op_simple to support to search for all images/videos/audio/pdfs.
   */
  function op_by_type() {
    if (empty($this->value)) {
      return;
    }
    $this->ensure_my_table();

    $value = is_array($this->value) ? array_pop($this->value) : $this->value;
    switch ($value) {
      case 'documents':
        $this->operator = 'IN';
        $value = array('.txt', '.html', '.doc', '.xls', '.pot', '.docx', '.xlsx', '.pptx', '.pdf', '.ppt', '.pps', '.odt', '.ods', '.odp');
        $value = array_map('file_get_mimetype', $value);
        $placeholder = '(' . db_placeholders($value, 'varchar') . ')';
        break;
      default:
        $this->operator = 'LIKE';
        $placeholder = "'%s%%'";
        $value = $value;
        break;
    }

    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field $this->operator $placeholder", $value);
  }
}